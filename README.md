# iGAS in PICUs in Scotland

## Description
Supporting material for study looking at burden of severe invasive group A streptococcal disease in children in Scotland. 

## Support
Please contact thomas.christie.williams@ed.ac.uk if you have any questions about this repository. 
